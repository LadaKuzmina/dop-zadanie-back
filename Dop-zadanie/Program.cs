using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Text;
using Ydb.Sdk;
using Ydb.Sdk.Auth;
using Ydb.Sdk.Services.Table;
using Ydb.Sdk.Value;
using Ydb.Sdk.Yc;

var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();
Guid guid = Guid.NewGuid();

app.MapPut("/api/Visit/{name}", async (string name) =>
{
    string dateOfVisit = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
    string message = $"Name: {name}, Date of Visit: {dateOfVisit}";
    Console.WriteLine(message);
    ICredentialsProvider provider = new MetadataProvider();
    await ((MetadataProvider)provider).Initialize();


    var config = new DriverConfig(
        "grpcs://ydb.serverless.yandexcloud.net:2135",
        "/ru-central1/b1guitmsnmqu9u39ujnh/etnhginbto0fhopa2o4d",
        provider
    );

    await Task.Delay(10000);
    var driver = new Driver(config);
    await driver.Initialize();
    var date = DateTime.Now;
    var id = Guid.NewGuid();
    const string query = $"""
                          DECLARE $id AS string;
                                         DECLARE $date AS Datetime;
                                          DECLARE $name AS String;
                          
                                      INSERT INTO table294 (id, date, name) VALUES
                                      ($id, $date, $name);

                          """;
    var parameters = new Dictionary<string, YdbValue>()
    {
        { "$name", YdbValue.MakeString(Encoding.Default.GetBytes(name)) },
        { "$id", YdbValue.MakeString(Encoding.Default.GetBytes(id.ToString())) },
        { "$date", YdbValue.MakeDatetime(date) },
    };
    var tableClient = new TableClient(driver, new TableClientConfig());
    var response = await tableClient.SessionExec(async session
        => await session.ExecuteDataQuery(
            query,
            TxControl.BeginSerializableRW().Commit(),
            parameters)
    );

    response.Status.EnsureSuccess();

    return Results.Ok(message);
});

app.MapGet("/api/Version", () =>
{
    string version = Environment.GetEnvironmentVariable("APP_VERSION") ?? "Version not set";
    return Results.Ok(version + " replica:" + guid.ToString());
});

app.Run();